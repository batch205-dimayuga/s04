class Student {
	constructor(name,email,grades) {
		this.name = name;
		this.email = email;
		this.average = undefined;
		this.isPassed = undefined;
		this.isPassedWithHonors = undefined;

		if(grades.some(grade => typeof grade !== 'number')){
			this.grades = undefined
		} else{
			this.grades = grades;
		}
	}

	login(){

        console.log(`${this.email} has logged in`);
        return this;
    }

    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
        return this;
    }

    computeAve(){
        let sum = this.grades.reduce((accumulator,num) => accumulator += num)
        this.average = Math.round(sum/4);
        return this;
    }

    willPass() {
        this.isPassed = Math.round(this.computeAve().average) >= 85 ? true : false;
        return this;
    }

    willPassWithHonors() {

       this.isPassedWithHonors = this.willPass().isPassed && Math.round(this.computeAve().average) >= 90 ? true : this.willPass().isPassed ? false : undefined;
       return this;
    }
}

// class Section will allow us to group our students as a section

class Section {

    // every instance of Section class will be instantiated with an empty array for our students.
    constructor(name){

        this.name = name;
        this.students = [];
        // add a counter or number of honor students:
        this.honorStudents = undefined;
        this.passedStudents = undefined;
        this.sectionAve = undefined;

    }

    // addStudents method will allow us to add instances of the Student class as items for our students property.
    addStudent(name,email,grades){

        // S student instance/object will b instantiated with the name,email,grades, and push into our students property
        this.students.push(new Student(name,email,grades));

        return this;
    }
    // countHonorStudents will loop over each Student instance in our students array and count the number of students who will pass with honors
    countHonorStudents(){
        //accumulate the number of honor students
        let count = 0;

        this.students.forEach(student => {
            // console.log(student);

            //log if each student isPassedWithHonors property
            // console.log(student.willPassWithHonors().isPassedWithHonors)

            // invoke will pass with honors so we can determine nd add whether the student passed with honors in its property
            student.willPassWithHonors();

            // console.log(student);

            // check if student isPassedWithHonors
            //add 1 ta temporary variable to hold the number of honorStudents
            if(student.isPassedWithHonors){
                count++
            }
        })

        //update honorStudents property with the updated value of count:
        this.honorStudents = count;
        return this;

    };

    getNumberOfStudents(){

        return this.students.length;

    };

    countPassedStudents(){
        let count = 0;
        this.students.forEach(student => {
            student.willPass();
            if(student.isPassed){
                count++
            }
        })
        this.passedStudents = count;
        return this;
    };

    computeSectionAve(){
        let sumGrades = 0;
        this.students.forEach(student => {
            student.computeAve();
            sumGrades += student.average
        })
        this.sectionAve = sumGrades/(this.students.length);
        return this;
    }

}

let section1A = new Section("Section1A");
console.log(section1A);

// 3 arguments that are needed to instantiate our Student
section1A.addStudent("Joy","joy@mail.com",[89,91,92,88]);
section1A.addStudent("Jeff","jeff@mail.com",[81,80,82,78]);
section1A.addStudent("John","john@mail.com",[91,90,92,96]);
section1A.addStudent("Jack","jack@mail.com",[95,92,92,93]);

console.log(section1A);

// check details of John from section1A
// console.log(section1A.students[2])

// check the average of John from section1A?
// console.log(section1A.students[2].computeAve().average)

// check if Jeff from section1A passed?
// console.log("Did Jeff pass?",section1A.students[1].computeAve().willPass().isPassed);

//display the number of honor students in our section
section1A.countHonorStudents();

//check the number of students:
console.log(section1A.countHonorStudents().honorStudents);

// Activity - Function Coding

section1A.addStudent("Alex","alex@mail.com",[84,85,85,86]);

